package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List<String> x, List<String> y) {
        // TODO: Implement the logic here
        outer:
            for (int i=0; i<x.size(); i++){
                String xString = x.get(i);
                for(int j = 0; j < y.size(); j++){
                    String yString = y.get(j);
                    if(!yString.equals(xString)){
                        y.remove(j--);
                    } else if(yString.equals(xString) && i == x.size() - 1){
                        return true;
                    } else {
                        continue outer;
                    }
                }
            }
        return false;
    }
    
    //
    
    
}
