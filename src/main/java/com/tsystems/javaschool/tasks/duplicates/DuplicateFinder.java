package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // TODO: Implement the logic here
        try {
            targetFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        String PATH_READ = sourceFile.getPath();
        String PATH_WRITE = targetFile.getPath();


        try {
            Stream<String> stream = Files.lines(Paths.get(PATH_READ), StandardCharsets.UTF_8);
            Map<String, Long> result = stream.collect(
                    Collectors.groupingBy(
                            Function.identity(), Collectors.counting()));

            List<String> result1 = result.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .map(entry -> entry.getKey() + "[" + entry.getValue() + "]")
                    .collect(Collectors.toList());
            Files.write(Paths.get(PATH_WRITE), result1);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


        return true;
    }


}
